package pl.arma.tud_hibernate.util;

import java.io.InputStream;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class EntityLoader {
	private static String loadJSON(Class entityClass, String filePath) {
		String jsonDataStr = "";
		
		try {
			InputStream entityDataStr = entityClass.getResourceAsStream(filePath);
			
			int nextByte;		
			do {
				nextByte = entityDataStr.read();
				if (nextByte != (-1)) 
					jsonDataStr += ((char)nextByte);
			}
			while (nextByte != (-1));
		}
		catch (Exception e) { }

		return jsonDataStr;
	}
	
	public static <T> T load(Class<T> entityClass, String filePath) {
		T loadedEntity = null;
		
		try {
			String jsonDataStr = loadJSON(entityClass, filePath);
			
			if (jsonDataStr.length() > 0) {
				Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
				loadedEntity = (T) gson.fromJson(jsonDataStr, entityClass);
			}
		}
		catch (Exception e) { }
		
		return loadedEntity;
	}

}
