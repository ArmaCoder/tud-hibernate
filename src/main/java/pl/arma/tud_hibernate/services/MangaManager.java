package pl.arma.tud_hibernate.services; 

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;

import pl.arma.tud_hibernate.entities.*;

@Component
@Transactional
public class MangaManager {

	@Autowired
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public void addManga(Manga manga) {
		sessionFactory.getCurrentSession().persist(manga);
	}
	
	public void removeManga(Manga manga) {
		sessionFactory.getCurrentSession().delete(manga);
	}
	
	public Manga getMangaById(Integer id) {
		return (Manga) sessionFactory.getCurrentSession().get(Manga.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<Manga> findMangasByPhrase(String phrase) {
		return (List<Manga>) sessionFactory.getCurrentSession()
				.getNamedQuery("manga.byPhrase")
				.setParameter("phrase", phrase)
				.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Manga> findMangasByScore(Double minimalScore) throws IllegalArgumentException {
		if (minimalScore >= 0.0 && minimalScore <= 5.0) {
			return (List<Manga>) sessionFactory.getCurrentSession()
					.getNamedQuery("manga.byScore")
					.setParameter("score", minimalScore)
					.list();
		}
		else throw new IllegalArgumentException("Minimal score must be between 0.0 and 5.0");
	}
	
	@SuppressWarnings("unchecked")
	public List<Manga> findMangasByTags(List<String> tagList) {
		List<Manga> relevantMangas = new ArrayList<>();
		
		for (String tag : tagList) {			
			List<Manga> mangaList = (List<Manga>) sessionFactory.getCurrentSession()
					.getNamedQuery("manga.byTag")
					.setParameter("tag", tag.replace(' ', '-'))
					.list();
			
			if (mangaList != null && mangaList.size() > 0)
				relevantMangas.addAll(mangaList);
		}
		
		return relevantMangas;
	}
	
	@SuppressWarnings("unchecked")
	public List<Manga> getAllMangas() {
		return (List<Manga>) sessionFactory.getCurrentSession()
				.getNamedQuery("manga.all")
				.list();
	}
}
