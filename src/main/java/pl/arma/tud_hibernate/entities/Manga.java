package pl.arma.tud_hibernate.entities;

import java.util.List;
import java.util.ArrayList;
import java.time.LocalDate;
import javax.persistence.*;

import org.apache.commons.collections.functors.FalsePredicate;

@Entity
@NamedQueries({
	@NamedQuery(
		name = "manga.byPhrase",
		query = "FROM Manga WHERE LOCATE(:phrase, title)>0 OR LOCATE(:phrase, synopsis)>0"
	),
	@NamedQuery(
		name = "manga.byScore",
		query = "SELECT DISTINCT man FROM Manga man, Review rev WHERE man.mangaId=rev.mangaId AND" +
				":score <= (SELECT avg((scoreForArt + scoreForPlot + scoreForDialogues) / 3) FROM Review WHERE mangaId=man.mangaId)"
	),
	@NamedQuery(
		name = "manga.byTag",
		query = "FROM Manga WHERE LOCATE(:tag, tags)>0"
	),
	@NamedQuery(
		name = "manga.all",
		query = "FROM Manga"
	)
})
public class Manga {
	private Integer mangaId;
	private String title;
	private Integer yearOfPublication;
	private String synopsis;
	private String tags;
	private List<Review> reviews = new ArrayList<>();
	
	@Override
	public boolean equals(Object o) {
		Manga otherManga = (Manga) o;
		
		boolean sameId =	mangaId == null ? 
							otherManga.getMangaId() == null : 
							mangaId.equals(otherManga.getMangaId());
		
		boolean sameTitle =	title == null ?
							otherManga.getTitle() == null :
							title.equals(otherManga.getTitle());
		
		boolean sameYoP	=	yearOfPublication == null ?
							otherManga.getYearOfPublication() == null :
							yearOfPublication.equals(otherManga.getYearOfPublication());
		
		boolean sameSyn =	synopsis == null ?
							otherManga.getSynopsis() == null :
							synopsis.equals(otherManga.getSynopsis());
		
		boolean sameTags =	tags == null ?
							otherManga.getTags() == null :
							tags.equals(otherManga.getTags());
		
		return (sameId && sameTitle && sameYoP && sameSyn && sameTags);
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "mangaId")
	public Integer getMangaId() {
		return mangaId;
	}

	public void setMangaId(Integer mangaId) throws UnsupportedOperationException {
		if (this.mangaId == null)
			this.mangaId = mangaId;
		else
			throw new UnsupportedOperationException("Cannot change primary keys.");
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) throws IllegalArgumentException {
		if (title.length() >= 3)
			this.title = title;
		else
			throw new IllegalArgumentException("The title must be at least 3 chars long");
	}

	public Integer getYearOfPublication() {
		return yearOfPublication;
	}

	public void setYearOfPublication(Integer yearOfPublication) throws IllegalArgumentException {
		if (yearOfPublication >= 1990 && yearOfPublication <= LocalDate.now().getYear())
			this.yearOfPublication = yearOfPublication;
		else
			throw new IllegalArgumentException("Year of publication must be between 1990 and now.");
	}

	@Column(length = 1024 * 1024)
	public String getSynopsis() {
		return synopsis;
	}

	public void setSynopsis(String synopsis) {
		this.synopsis = synopsis;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "mangaId")
	public List<Review> getReviews() {
		return reviews;
	}
	
	public void setReviews(List<Review> reviews) {
		this.reviews = new ArrayList<>(reviews);
	}
	
	public void addReview(Review review) {
		if (!this.reviews.contains(review))
			this.reviews.add(review);
	}
	
	public void removeReview(Review review) {
		this.reviews.remove(review);
	}
}
