package pl.arma.tud_hibernate.entities;

import java.util.Date;
import javax.persistence.*;

@Entity
public class Review {
	private Integer reviewId;
	private Integer mangaId;
	private Date dateOfPosting;
	private String title;
	private Double scoreForArt;
	private Double scoreForPlot;
	private Double scoreForDialogues;
	private String comments;

	@Override
	public boolean equals(Object o) {
		Review otherReview = (Review) o;
		
		boolean sameId =	reviewId == null ? 
							otherReview.getReviewId() == null : 
							reviewId.equals(otherReview.getReviewId());
		
		boolean sameTitle =	title == null ?
							otherReview.getTitle() == null :
							title.equals(otherReview.getTitle());
		
		boolean sameDoP	=	dateOfPosting == null ?
							otherReview.getDateOfPosting() == null :
							dateOfPosting.equals(otherReview.getDateOfPosting());
		
		boolean sameManga =	mangaId == null ?
							otherReview.getMangaId() == null :
							mangaId.equals(otherReview.getMangaId());
		
		boolean sameSFA =	scoreForArt == null ?
							otherReview.getScoreForArt() == null :
							scoreForArt.equals(otherReview.getScoreForArt());
		
		boolean sameSFP =	scoreForPlot == null ?
							otherReview.getScoreForPlot() == null :
							scoreForPlot.equals(otherReview.getScoreForPlot());
		
		boolean sameSFD =	scoreForDialogues == null ?
							otherReview.getScoreForDialogues() == null :
							scoreForDialogues.equals(otherReview.getScoreForDialogues());
		
		return (sameId && sameTitle && sameDoP && sameManga && 
				sameSFA && sameSFP && sameSFD);
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getReviewId() {
		return reviewId;
	}

	public void setReviewId(Integer reviewId) throws UnsupportedOperationException {
		if (this.reviewId == null)
			this.reviewId = reviewId;
		else
			throw new UnsupportedOperationException("Cannot change primary keys.");
	}
	
	public Integer getMangaId() {
		return mangaId;
	}

	public void setMangaId(Integer mangaId) {
		this.mangaId = mangaId;
	}

	@Temporal(TemporalType.DATE)
	public Date getDateOfPosting() {
		return dateOfPosting;
	}

	public void setDateOfPosting(Date dateOfPosting) throws IllegalArgumentException {
		if (!dateOfPosting.after(new Date()))
			this.dateOfPosting = dateOfPosting;
		else 
			throw new IllegalArgumentException("Date of posting cannot be future.");
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) throws IllegalArgumentException {
		if (title.length() >= 3)
			this.title = title;
		else
			throw new IllegalArgumentException("The title must be at least 3 chars long.");
	}

	public Double getScoreForArt() {
		return scoreForArt;
	}

	public void setScoreForArt(Double scoreForArt) throws IllegalArgumentException {
		if (scoreForArt >= 0.0 && scoreForArt <= 5.0)
			this.scoreForArt = scoreForArt;
		else
			throw new IllegalArgumentException("Score must be between 0.0 and 5.0.");
	}

	public Double getScoreForPlot() {
		return scoreForPlot;
	}

	public void setScoreForPlot(Double scoreForPlot) throws IllegalArgumentException {
		if (scoreForPlot >= 0.0 && scoreForPlot <= 5.0)
			this.scoreForPlot = scoreForPlot;
		else
			throw new IllegalArgumentException("Score must be between 0.0 and 5.0.");
	}

	public Double getScoreForDialogues() {
		return scoreForDialogues;
	}

	public void setScoreForDialogues(Double scoreForDialogues) throws IllegalArgumentException {
		if (scoreForDialogues >= 0.0 && scoreForDialogues <= 5.0)			
			this.scoreForDialogues = scoreForDialogues;
		else
			throw new IllegalArgumentException("Score must be between 0.0 and 5.0.");
	}

	@Column(length = 2 * 1024 * 1024)
	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
}
