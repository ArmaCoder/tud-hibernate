package pl.arma.tud_hibernate.services;

import static org.junit.Assert.*;

import java.util.List;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import pl.arma.tud_hibernate.entities.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/beans.xml")
@TransactionConfiguration(transactionManager = "txManager", defaultRollback = false)
@Transactional
public class UpdateTest extends UnitTestTemplate {
	// These define the subset of mock-ups to insert
	private static int MANGA_START_INDEX	= 2;
	private static int MANGA_STOP_INDEX		= 3;
	// These are for data integrity after the test
	private List<Manga> preexistingMangas	= null;
	private List<Review> preexistingReviews	= new ArrayList<>();
	
	@Autowired
	MangaManager mangaManager;
	
	@Before
	public void doBeforeTest() {
		cacheExistingData();
		insertMangas();
	}
	
	@After
	@Transactional
	public void cleanUpAfterTests() {
		// This attempts to delete all mangas and/or
		// reviews that may have been stored during
		// the tests.
		for (int i = MANGA_START_INDEX; i <= MANGA_STOP_INDEX; i++) {
			try {
				Manga toDelete = mockUpMangas.get(i);
				if (toDelete.getMangaId() != null)
					mangaManager.removeManga(mockUpMangas.get(i));
			}
			catch (Exception e) { }
		}
	}
	
	@Transactional
	public void insertMangas() {
		// Adding some new mangas from mock-up array
		// We're not testing the addition code here,
		// since we have an entire separate InsertionTest for that.
		for (int i = MANGA_START_INDEX; i <= MANGA_STOP_INDEX; i++)
			mangaManager.addManga(mockUpMangas.get(i));
	}
	
	@Transactional
	private void cacheExistingData() {
		// Caching all already existing entries 
		// for future integrity check
		preexistingMangas = mangaManager.getAllMangas();
		
		for (Manga m : preexistingMangas) {
			ArrayList<Review> temporary = new ArrayList<Review>(m.getReviews());
			if (temporary != null && temporary.size() > 0)
				preexistingReviews.addAll(temporary);
		}
	}
	
	@Test
	@Transactional
	public void modifyMangas() {
		Manga modifiedManga		= mockUpMangas.get(MANGA_STOP_INDEX);
		Review modifiedReview	= modifiedManga.getReviews().get(0);
		String newTitle			= "A Surely New Title.";
		String newBody			= "Yeah, something has changed!";
				
		// Let's modify the last inserted manga.
		modifiedManga.setTitle(newTitle);
		modifiedManga.setSynopsis(newBody);
		
		// Modify a review too	
		modifiedReview.setTitle(newTitle);
		modifiedReview.setComments(newBody);
		
		// Retrieve the records again to see if changes were stored		
		Manga perhapsModifiedManga		= mangaManager.getMangaById(mockUpMangas.get(MANGA_STOP_INDEX).getMangaId());			
		Review perhapsModifiedReview	= new ArrayList<>(perhapsModifiedManga.getReviews()).get(0);
		
		if (!modifiedManga.equals(perhapsModifiedManga))
			fail("Failed to modify: " + modifiedManga.getTitle());
		
		if (!modifiedReview.equals(perhapsModifiedReview))
			fail("Failed to modify: " + modifiedReview.getTitle());
		
		// Check if everything else stayed as it was
		if (preexistingMangas != null && preexistingMangas.size() > 0) {
			for (Manga m1 : preexistingMangas) {
				Manga m2 		= mangaManager.getMangaById(m1.getMangaId());
				List<Review> rl	= new ArrayList<>(m2.getReviews());
				
				if (!m1.equals(m2) || !preexistingReviews.containsAll(rl))
					fail("Some other data has accidentally been changed.");
			}
		}

	}

}