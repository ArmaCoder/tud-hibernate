package pl.arma.tud_hibernate.services;

import static org.junit.Assert.*;

import java.util.List;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import pl.arma.tud_hibernate.entities.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/beans.xml")
@TransactionConfiguration(transactionManager = "txManager", defaultRollback = false)
@Transactional
public class InsertionTest extends UnitTestTemplate {
	private static int MANGA_START_INDEX	= 0;
	private static int MANGA_STOP_INDEX		= 1;
	
	@Autowired
	MangaManager mangaManager;
	
	@After
	public void cleanUpAfterTests() {
		// This attempts to delete all mangas and/or
		// reviews that may have been stored during
		// the tests.
		for (int i = MANGA_START_INDEX; i <= MANGA_STOP_INDEX; i++) {
			try {
				Manga toDelete = mockUpMangas.get(i);
				if (toDelete.getMangaId() != null)
					mangaManager.removeManga(mockUpMangas.get(i));
			}
			catch (Exception e) { }
		}
	}

	@Test
	public void insertMangas() {
		// This inserts some preloaded mangas stored
		// in the UnitTestTemplate.mockUpMangas ArrayList.
		// The mangas have some reviews filled in already.
		//
		// See /src/test/java/.../services/UnitTestTemplate.java
		// for entity preloading code implementation and
		// /src/test/resources for mock-up test data.
		for (int i = MANGA_START_INDEX; i <= MANGA_STOP_INDEX; i++) {
			mangaManager.addManga(mockUpMangas.get(i));
		}
		
		// Let's check if both mangas and their reviews
		// have indeed been stored
		for (int i = MANGA_START_INDEX; i <= MANGA_STOP_INDEX; i++) {
			// Check integrity of mangas first
			Manga storedManga = mockUpMangas.get(i);
			Manga retrievedManga = null;
			
			if (storedManga.getMangaId() != null) {
				retrievedManga = mangaManager.getMangaById(storedManga.getMangaId());
				
				if (retrievedManga == null || !retrievedManga.equals(storedManga))
					fail("Stored copy of " + storedManga.getTitle() + " differs from retrieved one.");
			}
			else fail(storedManga.getTitle() + "has not been stored.");
			
			// Then check integrity of their reviews
			List<Review> storedReviews = storedManga.getReviews();
			List<Review> retrievedReviews = new ArrayList<>(retrievedManga.getReviews());
			
			if (storedReviews.size() != retrievedReviews.size())
				fail("Stored and retrieved review count mismatched.");
			for (Review r : retrievedReviews) {
				if (!storedReviews.contains(r)) fail("Review titled " + r.getTitle() + "was not found.");
			}
		}
	}

}
