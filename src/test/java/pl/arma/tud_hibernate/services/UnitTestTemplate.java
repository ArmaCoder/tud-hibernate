package pl.arma.tud_hibernate.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import pl.arma.tud_hibernate.entities.*;
import pl.arma.tud_hibernate.util.EntityLoader;

public class UnitTestTemplate {
	protected static List<Manga> mockUpMangas	= null;
	protected static List<Review> mockUpReviews	= null;
	
	static {
		loadTestData();
	}
	
	public static void loadTestData() {
		mockUpMangas = new ArrayList<>();
		mockUpReviews = new ArrayList<>();
		
		for (int i = 0; i < TestDataIndex.getMangaCount(); i++) {
			Manga manga = EntityLoader.load(Manga.class, TestDataIndex.getMangaTestDataPath(i));
			Review[] reviews = EntityLoader.load(Review[].class, TestDataIndex.getReviewTestDataPath(i));
			
			if (manga != null) {
				mockUpMangas.add(manga);
				
				if (reviews != null && reviews.length > 0) {
					mockUpReviews.addAll(Arrays.asList(reviews));
					manga.setReviews(Arrays.asList(reviews));
				}
			}
		}
	}
}
