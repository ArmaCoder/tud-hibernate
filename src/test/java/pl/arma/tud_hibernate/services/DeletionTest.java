package pl.arma.tud_hibernate.services;

import static org.junit.Assert.*;

import java.util.List;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import pl.arma.tud_hibernate.entities.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/beans.xml")
@TransactionConfiguration(transactionManager = "txManager", defaultRollback = false)
@Transactional
public class DeletionTest extends UnitTestTemplate {
	// These define the subset of mock-ups to insert
	private static int MANGA_START_INDEX	= 4;
	private static int MANGA_STOP_INDEX		= 5;
	// These are for data integrity after the test
	private List<Manga> preexistingMangas	= null;
	private List<Review> preexistingReviews	= new ArrayList<>();
	
	@Autowired
	MangaManager mangaManager;
	
	@Before
	public void doBeforeTest() {
		cacheExistingData();
		insertMangas();
	}
	
	@After
	@Transactional
	public void cleanUpAfterTests() {
		// This attempts to delete all mangas and/or
		// reviews that may have been stored during
		// the tests.
		for (int i = MANGA_START_INDEX; i <= MANGA_STOP_INDEX; i++) {
			try {
				Manga toDelete = mockUpMangas.get(i);
				if (toDelete.getMangaId() != null)
					mangaManager.removeManga(mockUpMangas.get(i));
			}
			catch (Exception e) { }
		}
	}
	
	@Transactional
	private void cacheExistingData() {
		// Caching all already existing entries 
		// for future integrity check
		preexistingMangas = mangaManager.getAllMangas();
		
		for (Manga m : preexistingMangas) {
			ArrayList<Review> temporary = new ArrayList<Review>(m.getReviews());
			if (temporary != null && temporary.size() > 0)
				preexistingReviews.addAll(temporary);
		}
	}
	
	@Transactional
	public void insertMangas() {
		// Adding some new mangas from mock-up array
		// We're not testing the addition code here,
		// since we have an entire separate InsertionTest for that.
		for (int i = MANGA_START_INDEX; i <= MANGA_STOP_INDEX; i++)
			mangaManager.addManga(mockUpMangas.get(i));
	}
	
	@Test
	@Transactional
	public void deletionTest() {
		// Obtain a sublist of mangas actually
		// inserted during this test's startup
		List<Manga> insertedMangas = mockUpMangas.subList(MANGA_START_INDEX, MANGA_STOP_INDEX + 1);
		
		// Try to delete the first one of them
		Manga deleteMe = insertedMangas.get(0);
		mangaManager.removeManga(deleteMe);
		
		// Check if the manga was really deleted
		Manga wasIDeleted = mangaManager.getMangaById(deleteMe.getMangaId());
		
		if (wasIDeleted != null)
			fail("Manga: " + wasIDeleted.getTitle() + " hasn't been deleted!");
		
		// Check to see if the other mangas stayed intact
		for (int i = 1; i < insertedMangas.size(); i++) {
			Manga thisOneShouldStay		= insertedMangas.get(i);
			Manga hopefullyThisExists	= mangaManager.getMangaById(thisOneShouldStay.getMangaId());
			
			if (hopefullyThisExists == null)
				fail("Deletion test accidentally removed: " + thisOneShouldStay.getTitle());
		}
		
		// Check to see if any data added prior to
		// launching this test was accidentally deleted
		for (Manga doNotTouchMe : preexistingMangas) {
			Manga doIStillExist = mangaManager.getMangaById(doNotTouchMe.getMangaId());
			
			if (doIStillExist == null)
				fail("Deletion test accidentally removed: " + doNotTouchMe.getTitle());
		}
	}
}
	

