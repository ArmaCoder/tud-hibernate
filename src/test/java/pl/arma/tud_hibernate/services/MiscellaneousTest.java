package pl.arma.tud_hibernate.services;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Set;
import java.util.ArrayList;
import java.util.HashMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import pl.arma.tud_hibernate.entities.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/beans.xml")
@TransactionConfiguration(transactionManager = "txManager", defaultRollback = false)
@Transactional
public class MiscellaneousTest extends UnitTestTemplate {
	// These define the subset of mock-ups to insert
	private static int MANGA_START_INDEX	= 6;
	private static int MANGA_STOP_INDEX		= TestDataIndex.getMangaCount() - 1;
	
	@Autowired
	MangaManager mangaManager;
	
	@Before
	@Transactional
	public void doBeforeTest() {
		// Since we're carrying out multiple tests
		// in this class, we need to reload test
		// data before any of them.
		loadTestData();
		
		// Adding some new mangas from mock-up array
		// We're not testing the addition code here,
		// since we have an entire separate InsertionTest for that.
		for (int i = MANGA_START_INDEX; i <= MANGA_STOP_INDEX; i++)
			mangaManager.addManga(mockUpMangas.get(i));
	}
	
	@After
	@Transactional
	public void cleanUpAfterTests() {
		// This attempts to delete all mangas and/or
		// reviews that may have been stored during
		// the tests.
		for (int i = MANGA_START_INDEX; i <= MANGA_STOP_INDEX; i++) {
			try {
				Manga toDelete = mockUpMangas.get(i);
				if (toDelete.getMangaId() != null)
					mangaManager.removeManga(mockUpMangas.get(i));
			}
			catch (Exception e) { }
		}
	}
	
	@Test
	public void findByScoreTest() {
		HashMap<Double, ArrayList<Manga>> mangaScoreMap = new HashMap<>();
		
		// Calculating average ratings for all
		// mangas inserted for this test
		for (int pass = 0; pass < 2; pass++) {
			for (int i = MANGA_START_INDEX; i <= MANGA_STOP_INDEX; i++) {
				Manga nextManga			= mockUpMangas.get(i);
				List<Review> reviews	= nextManga.getReviews();
				Double averageScore		= 0.0;
				
				for (Review r : reviews)
					averageScore += ((r.getScoreForArt() + r.getScoreForPlot() + r.getScoreForDialogues()) / 3);
				
				averageScore /= reviews.size();
				
				// The first pass is to populate the map
				// with all calculated average scores
				if (pass == 0) {
					mangaScoreMap.putIfAbsent(averageScore, new ArrayList<Manga>());
				}
				// The next pass is to populate all
				// scores with actual lists of mangas
				else {
					Set<Double> allScores = mangaScoreMap.keySet();					
					for (Double s : allScores) {
						if (s <= averageScore) mangaScoreMap.get(s).add(nextManga);
					}
				}
			}
		}
		
		// Checking the list of returned mangas from
		// the DB for all calculated minimal scores
		Set<Double> allAverageScores = mangaScoreMap.keySet();
		
		for (Double minimalScore : allAverageScores) {
			List<Manga> mangaListFromDB	= mangaManager.findMangasByScore(minimalScore);
			List<Manga> localMangaList	= mangaScoreMap.get(minimalScore);
			
			if (mangaListFromDB == null || !mangaListFromDB.containsAll(localMangaList))
				fail("Not all relevant mangas were returned by DB.");
		}
	}
	
	@Test
	public void findAllReviews() {
		for (int i = MANGA_START_INDEX; i <= MANGA_STOP_INDEX; i++) {
			Manga nextManga					= mockUpMangas.get(i);
			List<Review> localReviewList	= nextManga.getReviews();
			List<Review> remoteReviewList	= new ArrayList<Review>(
				mangaManager.getMangaById(nextManga.getMangaId()).getReviews()
			);
			
			if (localReviewList.size() != remoteReviewList.size() ||
				!localReviewList.containsAll(remoteReviewList)) 
			{
				fail("Not all inserted reviews were returned by DB.");
			}
		}
	}
	
	@Test
	public void findByTags() {
		HashMap<String, ArrayList<Manga>> mangaTagMap = new HashMap<>();
		
		// Extracting all possible tags from
		// the mangas inserted for this test
		for (int pass = 0; pass < 2; pass++) {
			for (int i = MANGA_START_INDEX; i <= MANGA_STOP_INDEX; i++) {
				Manga nextManga			= mockUpMangas.get(i);
				String[] allTags		= nextManga.getTags().split("\\s");
				
				// The first pass is to populate the map
				// with all tags
				if (pass == 0) {
					for (String tag : allTags)
						mangaTagMap.putIfAbsent(tag, new ArrayList<Manga>());
				}
				// The next pass is to populate all
				// tags with actual lists of mangas
				else {
					Set<String> tagSet = mangaTagMap.keySet();					
					for (String s : tagSet) {
						if (nextManga.getTags().contains(s)) mangaTagMap.get(s).add(nextManga);
					}
				}
			}
		}
		
		// Checking the list of returned mangas from
		// the DB for all extracted tags
		Set<String> allExtractedTags = mangaTagMap.keySet();
		
		for (String tag : allExtractedTags) {
			List<String> temporary		= new ArrayList<String>();
			temporary.add(tag);
			
			List<Manga> mangaListFromDB	= mangaManager.findMangasByTags(temporary);
			List<Manga> localMangaList	= mangaTagMap.get(tag);
			
			if (mangaListFromDB == null || !mangaListFromDB.containsAll(localMangaList))
				fail("Not all relevant mangas were returned by DB.");
		}
	}

}
