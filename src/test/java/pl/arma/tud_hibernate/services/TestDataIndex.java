package pl.arma.tud_hibernate.services;

public class TestDataIndex {
	private static String[] TEST_DATA_FILES		= {
		"/another.json",
		"/bleach.json",
		"/death_note.json",
		"/ghost_hunt.json",
		"/hellsing.json",
		"/higurashi.json",
		"/monster.json",
		"/naruto.json",
		"/shiki.json",
		"/zombie_loan.json"
	};
	
	public static int getMangaCount() {
		return TEST_DATA_FILES.length;
	}
	
	public static String getMangaTestDataPath(int i) {
		if (i >= 0 && i <= 9)
			return "/mangas/" + TEST_DATA_FILES[i];
		else
			return null;
	}
	
	public static String getReviewTestDataPath(int i) {
		if (i >= 0 && i <= 9)
			return "/reviews/" + TEST_DATA_FILES[i];
		else
			return null;
	}
}
